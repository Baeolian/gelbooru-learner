﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Windows.Forms;
using System.Data.SQLite;
using System.Threading;
using System.Runtime.InteropServices;
using System.ComponentModel;
using System.Drawing;
using System.Linq;

namespace SimpleLearner
{
    public partial class Main : Form
    {
        String current_image;
        int current_image_id;
        private volatile Json_Received jrr;
        int progreso;

        public Main()
        {
            InitializeComponent();
            pictureBox1.WaitOnLoad = false;
            pictureBox1.LoadCompleted += pictureBox1_LoadCompleted;
            pictureBox3.Visible = true;
            init_database();
            disable_buttons();
            getRandomImage();
        }

        private void disable_buttons()
        {
            btn_hate.Enabled = false;
            btn_hate.BackColor = Color.LightGray;
            btn_love.Enabled = false;
            btn_love.BackColor = Color.LightGray;
            btn_next.Enabled = false;
            btn_next.BackColor = Color.LightGray;
            btn_no.Enabled = false;
            btn_no.BackColor = Color.LightGray;
            btn_yes.Enabled = false;
            btn_yes.BackColor = Color.LightGray;
        }

        private void enable_buttons()
        {
            btn_hate.Enabled = true;
            btn_hate.BackColor = Color.Black;
            btn_love.Enabled = true;
            btn_love.BackColor = Color.HotPink;
            btn_next.Enabled = true;
            btn_next.BackColor = Color.DodgerBlue;
            btn_no.Enabled = true;
            btn_no.BackColor = Color.DarkRed;
            btn_yes.Enabled = true;
            btn_yes.BackColor = Color.ForestGreen;
            pictureBox3.Visible = false;
        }

        void pictureBox1_LoadCompleted(object sender, AsyncCompletedEventArgs e)
        {
            enable_buttons();
            List<String> image_tags = jrr.tags.Split(' ').ToList();
            var result = image_tags.Select(s => new { value = s }).ToList();
            dataGridView1.DataSource = result;
        }

        private void btn_hate_Click(object sender, EventArgs e)
        {
            disable_buttons();
            new Thread(() => update_tags(jrr, 1)).Start();
            getRandomImage();
            pictureBox3.Visible = true;
        }

        private void btn_no_Click(object sender, EventArgs e)
        {
            disable_buttons();
            new Thread(() => update_tags(jrr, 2)).Start();
            getRandomImage();
            pictureBox3.Visible = true;
        }

        private void btn_yes_Click(object sender, EventArgs e)
        {
            disable_buttons();
            new Thread(() => update_tags(jrr, 3)).Start();
            getRandomImage();
            pictureBox3.Visible = true;
        }

        private void btn_love_Click(object sender, EventArgs e)
        {
            disable_buttons();
            new Thread(() => update_tags(jrr, 4)).Start();
            getRandomImage();
            pictureBox3.Visible = true;
        }

        private void btn_next_Click(object sender, EventArgs e)
        {            
            disable_buttons();
            getRandomImage();
            pictureBox3.Visible = true;
        }

        private void btn_download_Click(object sender, EventArgs e)
        {
            using (SaveFileDialog saveFileDialog = new SaveFileDialog() { Filter = @"All files (*.*)|*.*" })
            {
                saveFileDialog.FileName = current_image_id + "" + Path.GetExtension(current_image);
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    pictureBox1.Image.Save(saveFileDialog.FileName);
                }
            }
        }

        private void btn_ranking_Click(object sender, EventArgs e)
        {
            new Thread(() => new Tags().ShowDialog()).Start();
        }

        public void getRandomImage()
        {
            if (progreso < 30)
            {
                new Thread(get0_30Image).Start();
            }
            else if (progreso < 60)
            {
                new Thread(get30_60Image).Start();
            }
            else if (progreso < 80)
            {
                new Thread(get60_80Image).Start();
            }
            else
            {
                new Thread(get80_100Image).Start();
            }
        }

        public void get0_30Image()
        {
            String api_latest = "https://gelbooru.com/index.php?page=dapi&s=post&q=index&limit=1&json=1&api_key=5b5fc7120397f3f05721e50d5a143e519e0802ea56d814a3e121342313db8395&user_id=823071";
            var json_latest = new WebClient().DownloadString(api_latest);
            Json_Received jrl = JsonConvert.DeserializeObject<List<Json_Received>>(json_latest)[0];
            var latest_id = Int32.Parse(jrl.id);
            Random rnd = new Random();
            int random_id = rnd.Next(1, latest_id);

            String api_random = "https://gelbooru.com/index.php?page=dapi&s=post&q=index&id=" + random_id + "&json=1&api_key=5b5fc7120397f3f05721e50d5a143e519e0802ea56d814a3e121342313db8395&user_id=823071";
            var json_random = new WebClient().DownloadString(api_random);
            try
            {
                jrr = JsonConvert.DeserializeObject<List<Json_Received>>(json_random)[0];
                this.Invoke((MethodInvoker)delegate {
                    pictureBox1.LoadAsync(jrr.file_url);
                    check_database();
                });

                current_image = jrr.file_url;
                current_image_id = random_id;
                update_tags(jrr, 0);                
            }
            catch
            {
                get0_30Image();
            }
        }

        public void get30_60Image()
        {
            string cs = @"URI=file:tags.db";

            var con = new SQLiteConnection(cs);
            con.Open();

            var cmd = new SQLiteCommand(con);

            cmd.CommandText = "SELECT * FROM tags ORDER BY value DESC LIMIT 30";
            SQLiteDataReader rdr = cmd.ExecuteReader();

            List<String> etiquetas = new List<String>();

            int i = 0;
            while (rdr.Read())
            {
                etiquetas.Add(rdr.GetString(1));
                i++;
            }

            String builder = "https://gelbooru.com/index.php?page=dapi&s=post&q=index&limit=300&tags=";

            Random rnd = new Random();
            List<int> aleatorios = new List<int>();

            for (int a = 0; a < 6; a++)
            {
                aleatorios.Add(rnd.Next(0, 30));
            }

            for (int a = 0; a < 6; a++)
            {
                if (!builder.Contains(etiquetas[aleatorios[a]]))
                {
                    builder += etiquetas[aleatorios[a]];
                    if (a < 5)
                    {
                        builder += "+";
                    }                    
                }
            }

            if (builder.EndsWith("+"))
            {
                builder = builder.Remove(builder.Length - 1);
            }

            builder += "+sort:random&json=1&api_key=5b5fc7120397f3f05721e50d5a143e519e0802ea56d814a3e121342313db8395&user_id=823071";

            var json_search = new WebClient().DownloadString(builder);
            try
            {
                jrr = JsonConvert.DeserializeObject<List<Json_Received>>(json_search)[rnd.Next(0,300)];
                this.Invoke((MethodInvoker)delegate {
                    pictureBox1.LoadAsync(jrr.file_url);
                    check_database();
                });

                var id = Int32.Parse(jrr.id);
                current_image = jrr.file_url;
                current_image_id = id;
                update_tags(jrr, 0);                
            }
            catch
            {
                get30_60Image();
            }
        }

        public void get60_80Image()
        {
            string cs = @"URI=file:tags.db";

            var con = new SQLiteConnection(cs);
            con.Open();

            var cmd = new SQLiteCommand(con);

            cmd.CommandText = "SELECT * FROM tags ORDER BY value DESC LIMIT 20";
            SQLiteDataReader rdr = cmd.ExecuteReader();

            List<String> etiquetas = new List<String>();

            int i = 0;
            while (rdr.Read())
            {
                etiquetas.Add(rdr.GetString(1));
                i++;
            }

            String builder = "https://gelbooru.com/index.php?page=dapi&s=post&q=index&limit=200&tags=";

            Random rnd = new Random();
            List<int> aleatorios = new List<int>();

            for (int a = 0; a < 8; a++)
            {
                aleatorios.Add(rnd.Next(0, 20));
            }

            for (int a = 0; a < 8; a++)
            {
                if (!builder.Contains(etiquetas[aleatorios[a]]))
                {
                    builder += etiquetas[aleatorios[a]];
                    if (a < 7)
                    {
                        builder += "+";
                    }
                }
            }

            if (builder.EndsWith("+"))
            {
                builder = builder.Remove(builder.Length - 1);
            }

            builder += "+sort:random&json=1&api_key=5b5fc7120397f3f05721e50d5a143e519e0802ea56d814a3e121342313db8395&user_id=823071";

            var json_search = new WebClient().DownloadString(builder);
            try
            {
                jrr = JsonConvert.DeserializeObject<List<Json_Received>>(json_search)[rnd.Next(0, 200)];
                this.Invoke((MethodInvoker)delegate {
                    pictureBox1.LoadAsync(jrr.file_url);
                    check_database();
                });

                var id = Int32.Parse(jrr.id);
                current_image = jrr.file_url;
                current_image_id = id;
                update_tags(jrr, 0);
            }
            catch
            {
                get60_80Image();
            }
        }

        public void get80_100Image()
        {
            string cs = @"URI=file:tags.db";

            var con = new SQLiteConnection(cs);
            con.Open();

            var cmd = new SQLiteCommand(con);

            cmd.CommandText = "SELECT * FROM tags ORDER BY value DESC LIMIT 15";
            SQLiteDataReader rdr = cmd.ExecuteReader();

            List<String> etiquetas = new List<String>();

            int i = 0;
            while (rdr.Read())
            {
                etiquetas.Add(rdr.GetString(1));
                i++;
            }

            String builder = "https://gelbooru.com/index.php?page=dapi&s=post&q=index&limit=200&tags=";

            Random rnd = new Random();
            List<int> aleatorios = new List<int>();

            for (int a = 0; a < 10; a++)
            {
                aleatorios.Add(rnd.Next(0, 15));
            }

            for (int a = 0; a < 10; a++)
            {
                if (!builder.Contains(etiquetas[aleatorios[a]]))
                {
                    builder += etiquetas[aleatorios[a]];
                    if (a < 9)
                    {
                        builder += "+";
                    }
                }
            }

            if (builder.EndsWith("+"))
            {
                builder = builder.Remove(builder.Length - 1);
            }

            builder += "+sort:random&json=1&api_key=5b5fc7120397f3f05721e50d5a143e519e0802ea56d814a3e121342313db8395&user_id=823071";

            var json_search = new WebClient().DownloadString(builder);
            try
            {
                jrr = JsonConvert.DeserializeObject<List<Json_Received>>(json_search)[rnd.Next(0, 200)];
                this.Invoke((MethodInvoker)delegate {
                    pictureBox1.LoadAsync(jrr.file_url);
                    check_database();
                });

                var id = Int32.Parse(jrr.id);                
                current_image = jrr.file_url;
                current_image_id = id;
                update_tags(jrr, 0);
            }
            catch
            {
                get80_100Image();
            }
        }

        private void update_tags(Json_Received jr, int ctrl)
        {
            String tags_str = jr.tags;
            String[] tags = tags_str.Split(' ');

            switch (ctrl)
            {
                case 0: //Random || Next
                    update_database(tags);
                    break;

                case 1: //Hate (-2)
                    update_database(tags);
                    hate_database(tags);
                    break;

                case 2: //No (-1)
                    update_database(tags);
                    no_database(tags);
                    break;

                case 3: //Yes (+1)
                    update_database(tags);
                    yes_database(tags);
                    break;

                case 4: //Love (+2)
                    update_database(tags);
                    love_database(tags);
                    break;
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://gelbooru.com/index.php?page=post&s=view&id=" + current_image_id);
        }

        private void init_database()
        {
            string cs = @"URI=file:tags.db";

            var con = new SQLiteConnection(cs);
            con.Open();

            var cmd = new SQLiteCommand(con);

            cmd.CommandText = @"CREATE TABLE IF NOT EXISTS tags(id INTEGER PRIMARY KEY, name TEXT, value INT)";
            cmd.ExecuteNonQuery();            

            con.Close();
        }

        private void check_database()
        {
            string cs = @"URI=file:tags.db";

            var con = new SQLiteConnection(cs);
            con.Open();

            var cmd = new SQLiteCommand(con);

            cmd.CommandText = "SELECT * FROM tags ORDER BY value DESC LIMIT 15";
            SQLiteDataReader rdr = cmd.ExecuteReader();

            int suma = 0;

            while (rdr.Read())
            {
                suma += rdr.GetInt32(2);
            }

            suma = Math.Abs((int)(suma / 15));
            progreso = suma;

            if(suma < 100)
            {
                progressBar1.Value = suma;
                label6.Text = "AI Trained " + suma.ToString() + " %";
            }
            else
            {
                progressBar1.Value = 100;
                label6.Text = "AI Trained 100 %";
            }

            if (suma < 30)
            {
                progressBar1.SetState(2);
            }
            else if (suma < 60)
            {
                progressBar1.SetState(3);
            }
            else
            {
                progressBar1.SetState(1);
            }


            con.Close();
        }

        private void update_database(String[] tags)
        {
            string cs = @"URI=file:tags.db";

            var con = new SQLiteConnection(cs);
            con.Open();

            var cmd = new SQLiteCommand(con);

            foreach (var tag in tags)
            {
                cmd.CommandText = "SELECT count(*) FROM tags WHERE name='" + tag + "'";
                int existe = Convert.ToInt32(cmd.ExecuteScalar());
                if (existe == 0)
                {
                    cmd.CommandText = "INSERT INTO tags(name, value) VALUES ('" + tag + "', 0)";
                    cmd.ExecuteNonQuery();
                }
            }
            con.Close();
        }

        private void hate_database(String[] tags)
        {
            string cs = @"URI=file:tags.db";

            var con = new SQLiteConnection(cs);
            con.Open();

            var cmd = new SQLiteCommand(con);

            foreach (var tag in tags)
            {
                cmd.CommandText = "UPDATE tags SET value = value - 2 WHERE name='" + tag + "'";
                cmd.ExecuteNonQuery();
            }
            con.Close();
        }

        private void no_database(String[] tags)
        {
            string cs = @"URI=file:tags.db";

            var con = new SQLiteConnection(cs);
            con.Open();

            var cmd = new SQLiteCommand(con);

            foreach (var tag in tags)
            {
                cmd.CommandText = "UPDATE tags SET value = value - 1 WHERE name='" + tag + "'";
                cmd.ExecuteNonQuery();
            }
            con.Close();
        }

        private void yes_database(String[] tags)
        {
            string cs = @"URI=file:tags.db";

            var con = new SQLiteConnection(cs);
            con.Open();

            var cmd = new SQLiteCommand(con);

            foreach (var tag in tags)
            {
                cmd.CommandText = "UPDATE tags SET value = value + 1 WHERE name='" + tag + "'";
                cmd.ExecuteNonQuery();
            }
            con.Close();
        }

        private void love_database(String[] tags)
        {
            string cs = @"URI=file:tags.db";

            var con = new SQLiteConnection(cs);
            con.Open();

            var cmd = new SQLiteCommand(con);

            foreach (var tag in tags)
            {
                cmd.CommandText = "UPDATE tags SET value = value + 2 WHERE name='" + tag + "'";
                cmd.ExecuteNonQuery();
            }
            con.Close();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://gelbooru.com/");
        }
    }

    public class Json_Received
    {
        public string source { get; set; }
        public string directory { get; set; }
        public string hash { get; set; }
        public string height { get; set; }
        public string id { get; set; }
        public string image { get; set; }
        public string change { get; set; }
        public string owner { get; set; }
        public string parent_id { get; set; }
        public string rating { get; set; }
        public string sample { get; set; }
        public string preview_height { get; set; }
        public string preview_width { get; set; }
        public string sample_height { get; set; }
        public string sample_width { get; set; }
        public string score { get; set; }
        public string tags { get; set; }
        public string title { get; set; }
        public string width { get; set; }
        public string file_url { get; set; }
        public string created_at { get; set; }
        public string post_locked { get; set; }
    }

    public static class ModifyProgressBarColor
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = false)]
        static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr w, IntPtr l);
        public static void SetState(this ProgressBar pBar, int state)
        {
            SendMessage(pBar.Handle, 1040, (IntPtr)state, IntPtr.Zero);
        }
    }
}
