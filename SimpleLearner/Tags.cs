﻿using System;
using System.Data;
using System.Data.SQLite;
using System.Windows.Forms;

namespace SimpleLearner
{
    public partial class Tags : Form
    {
        public Tags()
        {
            InitializeComponent();
            update_DBview();
        }

        public void update_DBview()
        {
            string cs = @"URI=file:tags.db";

            var con = new SQLiteConnection(cs);
            con.Open();

            var cmd = new SQLiteCommand(con);

            cmd.CommandText = "SELECT * FROM tags ORDER BY value DESC";

            SQLiteDataAdapter adapt = new SQLiteDataAdapter();
            adapt.SelectCommand = cmd;

            try
            {
                DataTable dt = new DataTable();
                adapt.Fill(dt);

                dt.Columns.Remove("id");

                dataGridView1.DataSource = dt;
            }
            catch
            {

            }            

            con.Close();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int selected_cells = dataGridView1.GetCellCount(DataGridViewElementStates.Selected);
            if (selected_cells == 2)
            {
                button1.Enabled = true;
                button1.Text = "Remove '" + dataGridView1.SelectedCells[0].Value.ToString() + "' tag";
            }
            else
            {
                button1.Enabled = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Removing tag '" + dataGridView1.SelectedCells[0].Value.ToString() + "' from DB", "Are you sure?", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                delete_tagDB();
                update_DBview();
            }
            else if (dialogResult == DialogResult.No)
            {
                //Close
            }
        }

        private void delete_tagDB()
        {
            string cs = @"URI=file:tags.db";

            var con = new SQLiteConnection(cs);
            con.Open();

            var cmd = new SQLiteCommand(con);

            cmd.CommandText = "DELETE FROM tags WHERE name = '" + dataGridView1.SelectedCells[0].Value.ToString() + "'";

            cmd.ExecuteNonQuery();

            con.Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if (row.Cells[0].Value.ToString().Contains(textBox1.Text))
                {
                    dataGridView1.CurrentCell = dataGridView1.Rows[row.Index].Cells[0];
                    break;
                }
            }
        }
    }
}
